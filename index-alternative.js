const express = require("express");
const app = express();

// Set default port
const port = 8080;

// Set conf for view
app.use(express.static(__dirname));
app.set('views', __dirname);
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Get first video in 720P
app.get('/video-1', function (req, res) {
  res.render('index-alternative.html', {
    video: '720P_VIDEO_ULTRAHD_120FPS.mp4'
  });
});

// Get second video in 8K
app.get('/video-2', function (req, res) {
  res.render('index-alternative.html', {
    video: '8K_VIDEO_ULTRAHD_120FPS.webm'
  });
});


// Start server
app.listen(port, function () {
  console.log("Server start on " + port);
});

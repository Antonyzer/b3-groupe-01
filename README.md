# Group Project 1

## How to run

- Run ```npm i``` to install dependencies,
- Set your videos into **videos** directory,
- Run ```npm run start``` to launch server.

## Routes

- [720P](http://localhost:8080/video-1)
- [8K](http://localhost:8080/video-2)

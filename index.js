const express = require("express");
const fs = require('fs');
const app = express();

// Set default port
const port = 8080;

// Set conf for view
app.use(express.static(__dirname));
app.set('views', __dirname);
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Get first video in 720P
app.get('/video-1', function (req, res) {
  res.render('index.html', {
      video: '720P_VIDEO_ULTRAHD_120FPS.mp4'
  });
});

// Get second video in 8K
app.get('/video-2', function (req, res) {
  res.render('index.html', {
      video: '8K_VIDEO_ULTRAHD_120FPS.webm'
  });
});

// Get video flux
app.get("/video/:name", function (req, res) {
  // Get current filename
  const filename = req.params.name;

  // Check file exists
  if (fs.existsSync('videos/' + filename)) {
    // Ensure there is a range given for the video
    const range = req.headers.range;
    if (!range) {
      res.status(400).send("Requires Range header");
    }

    // Get video path and size
    const videoPath = "videos/" + filename;
    const videoSize = fs.statSync("videos/" + filename).size;

    // Parse Range
    const CHUNK_SIZE = 10 ** 6; // 1MB
    const start = Number(range.replace(/\D/g, ""));
    const end = Math.min(start + CHUNK_SIZE, videoSize - 1);

    // Create headers
    const contentLength = end - start + 1;
    const headers = {
      "Content-Range": `bytes ${start}-${end}/${videoSize}`,
      "Accept-Ranges": "bytes",
      "Content-Length": contentLength,
      "Content-Type": "video/mp4",
    };

    // HTTP Status 206 for Partial Content : https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range
    res.writeHead(206, headers);

    // Create video stream
    const videoStream = fs.createReadStream(videoPath, { start, end });

    // Stream the video chunk
    videoStream.pipe(res);
  }
});

// Start server
app.listen(port, function () {
  console.log("Server start on " + port);
});
